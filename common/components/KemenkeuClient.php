<?php

namespace common\components;

use yii\authclient\OpenIdConnect;
use dektrium\user\clients\ClientInterface;

/**
 * Client auth for Kemenkeu SSO
 * @author Ardhian Dharma
 */
class KemenkeuClient extends OpenIdConnect implements ClientInterface
{

    public $authUrl = 'https://sso.kemenkeu.go.id/connect/authorize';
    public $tokenUrl = 'https://sso.kemenkeu.go.id/connect/token';
    public $apiBaseUrl = 'https://sso.kemenkeu.go.id/connect';

    protected function initUserAttributes()
    {
        $user = $this->api('userinfo', 'GET');
        $user['id'] = $user['sub'];
        return $user;
    }

    public function getEmail()
    {
        return $this->getUserAttributes()['email'];
    }

    public function getUsername()
    {
        return $this->getUserAttributes()['preferred_username'];
    }

    protected function defaultName()
    {
        return 'live';
    }
}
