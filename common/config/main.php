<?php

use yii\helpers\Json;

return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager'
        ],
        'authClientCollection' => [
            'class'   => \yii\authclient\Collection::className(),
            'clients' => [
                // please change clientId and clientSecret
                'sso' => [
                    'class' => 'common\components\KemenkeuClient',
                    'clientId' => 'your-client-id',
                    'clientSecret' => 'your-client-secret',
                    'issuerUrl' => 'https://sso.kemenkeu.go.id',
                    'scope' => 'openid profile profil.hris'
                ],
            ],
        ],
    ],
    'modules' => [
        'user' => [
            'class' => 'dektrium\user\Module',
            'admins' => ['administrator'],
            'enableConfirmation' => false,
            'enablePasswordRecovery' => false,
            'modelMap' => [
                'User' => 'common\models\User',
                'RegistrationForm' => 'common\models\RegistrationForm',
            ],
        ],
        'admin' => [
            'class' => 'mdm\admin\Module',
            'controllerMap' => [
                'assignment' => [
                    'class' => 'mdm\admin\controllers\AssignmentController',
                    'extraColumns' => [
                        [
                            'attribute' => 'name',
                            'label' => 'Name',
                            'value' => function ($model, $key, $index, $column) {
                                $name = '-';
                                if (!empty($model->accounts)) {
                                    $name = Json::decode($model->accounts['sso']->data)['name'];
                                }
                                return $name;
                            },
                        ],
                    ],
                ],
            ],
        ],
    ],
];
