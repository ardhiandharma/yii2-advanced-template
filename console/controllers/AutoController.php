<?php

namespace console\controllers;

use common\models\User;
use yii\console\Controller;

class AutoController extends Controller
{

    public function actionIndex()
    {
        $this->fillUserInformation();
    }

    private function fillUserInformation()
    {
        foreach (User::find()->all() as $user) {
            if (is_null($user->getAccountByProvider('sso'))) {
                continue;
            }
            echo "Processing " . $user->username . "\n";
            $user->profile->name = $user->fullName;
            $user->profile->location = trim($user->unit);
            $user->save();
            $user->profile->save();
        }
    }
}
